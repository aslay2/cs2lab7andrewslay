package edu.westga.cs1302.currencyconverter.viewmodel;

import edu.westga.cs1302.currencyconverter.model.ConversionRate;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * ViewModel class.
 * @author Andrew Slay
 * @version CS1302
 *
 */
public class ViewModel {

	private StringProperty amountProperty;
	private StringProperty currencyProperty;
	private StringProperty resultProperty;
	private StringProperty errorMessage;
	
	/**
	 * Viewmodel instantiation.
	 */
	public ViewModel() {
		this.amountProperty = new SimpleStringProperty();
		this.currencyProperty = new SimpleStringProperty();
		this.resultProperty = new SimpleStringProperty();
		this.errorMessage = new SimpleStringProperty();
	}
	
	/**
	 * Gets amount property.
	 * @return the amount property
	 */
	public StringProperty getAmountProperty() {
		return this.amountProperty;
	}
	
	/**
	 * Gets currency property.
	 * @return the currency property
	 */
	public StringProperty getCurrencyProperty() {
		return this.currencyProperty;
	}
	
	/**
	 * Gets result property.
	 * @return resultProperty the result property
	 */
	public StringProperty getResultProperty() {
		return this.resultProperty;
	}

	/**
	 * Gets error property.
	 * @return errorMessage the error property
	 */
	public StringProperty getErrorMessage() {
		return this.errorMessage;
	}

	/**
	 * Gets enum and returns it. Returns null if nonexistent.
	 * 
	 * @precondition enumText != null
	 * @postcondition none
	 * @param enumText the text to match with an enum value
	 * @return null if nonexistent, the matching enum value if exists
	 */
	public ConversionRate checkValidConversionRate(String enumText) {
		if (enumText == null) {
			return null;
		}
		
		for (ConversionRate rate : ConversionRate.values()) {
			if (rate.name().matches(enumText)) {
				return rate;
			}
		}
		
		return null;
	}
	
	/**
	 * Converts value and sets property
	 * @precondition value > 0
	 * @postcondition result property is set to conversion
	 * @param value the value to exchange
	 * @param rate the rate of exchange to convert
	 */
	public void convert(double value, String rate) {
		if (value <= 0) {
			this.errorMessage.set("Error: the value should be above zero.");
			return;
		}
		if (this.checkValidConversionRate(rate) == null) {
			this.errorMessage.set("Error: The conversion rate is invalid.");
			return;
		}
		
		this.resultProperty.set("$" + value + " = " + this.checkValidConversionRate(rate).exchange(value) + rate);
	}

}
