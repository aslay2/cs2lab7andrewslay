package edu.westga.cs1302.currencyconverter.view;

import edu.westga.cs1302.currencyconverter.viewmodel.ViewModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

/**
 * CodeBehind Class
 * @author Andrew Slay
 * @version CS1302
 */
public class CodeBehind {

	@FXML
	private TextField usdBox;
	
	@FXML
	private TextField convertBox;
	
	@FXML
	private Label amountHeader;
	
	@FXML 
	private Text errorBox;
	
	@FXML
	private Label convertHeader;
	
	@FXML
	private Button convertButton;
	
	@FXML
	private TextArea convertedField;
	
	@FXML
	private Label currencyDesc;
	
	private ViewModel viewModel;
	
	/**
	 * Code behind method.
	 */
	public CodeBehind() {
		this.viewModel = new ViewModel();
	}
	
	@FXML
	private void initialize() {
		this.convertedField.setEditable(false);
		this.convertedField.textProperty().bindBidirectional(this.viewModel.getResultProperty());
		this.usdBox.textProperty().bindBidirectional(this.viewModel.getAmountProperty());
		this.errorBox.textProperty().bindBidirectional(this.viewModel.getErrorMessage());
		this.convertBox.textProperty().bindBidirectional(this.viewModel.getCurrencyProperty());
	}
	
	@FXML
	private void handleConversion(ActionEvent event) {
		try {
			Double priceToConvert = Double.parseDouble(this.usdBox.getText());
			this.viewModel.convert(priceToConvert, this.convertBox.getText());
		} catch (NullPointerException | NumberFormatException theException) {
			theException.printStackTrace();
		}
	}
}
	
